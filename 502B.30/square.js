import { Retangle } from "./retangle.js";

class Square extends Retangle {
    _description;

    constructor(paramLength, paramDescription = "Đây là hình vuông") {
        super(paramLength, paramLength);

        this._description = paramDescription;
    }

    getDescription() {
        return this._description;
    }

    getPerimeter() {
        return 4 * this._width;
    }
}

export { Square };