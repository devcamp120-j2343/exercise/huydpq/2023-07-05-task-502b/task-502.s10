import { Retangle } from "./retangle.js";
import { Square } from "./square.js";

var retangle1 = new Retangle(5, 10);
var retangle2 = new Retangle(10, 20);

console.log(retangle1.getArea());
console.log(retangle2.getArea());

var square1 = new Square(5);
var square2 = new Square(10, "Hình vuông");

console.log(square1.getArea());
console.log(square2.getArea());

console.log(square1.getDescription());
console.log(square2.getDescription());

console.log(square1.getPerimeter());
console.log(square2.getPerimeter());

console.log(retangle1 instanceof Retangle);
console.log(retangle1 instanceof Square);
console.log(square1 instanceof Retangle);
console.log(square1 instanceof Square);

// Hoisting
console.log(sumNumber(1,2));
function sumNumber (a,b) {
    return  a+b;
};