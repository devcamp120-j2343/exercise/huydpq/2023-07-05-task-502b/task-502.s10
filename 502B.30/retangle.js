class Retangle {
    _height;
    _width;

    constructor(paramHeight = 0, paramWidth = 0) {
        this._height = paramHeight;
        this._width = paramWidth;
    }

    getArea() {
        return this._height * this._width;
    }
}

export { Retangle }